/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <Kokkos_Core.hpp>
#include <Kokkos_UnorderedMap.hpp>
#include <Kokkos_Array.hpp>
#include <amr/AmrCore.hpp>
#include <io/IniFile.hpp>

// ************************************************************************
// Namespace for a dummy work kernel: heat stencil + work_load
namespace WorkKernel {

  struct Param {

    real_type kappa1;
    real_type kappa2;
    real_type cfl;

    Param(IO::IniFile iparam)
      : kappa1(), kappa2(), cfl()
    {
      kappa1 = iparam.get("work","kappa1",1.);
      kappa2 = iparam.get("work","kappa2",1.);
      cfl    = iparam.get("work","cfl",0.5);
    }

  };

  // ************************************************************************
  // Kernel task to perform work on a leaf
  struct WorkTask {
    using value_type = void;
    using wparam_type = Param;

    int id;
    mesh_type mesh;

    Kokkos::Array<int,24> index_neighbors;
    view_type ghostview;

    key_type key;
    block_type block;
    wparam_type wparam;

    KOKKOS_INLINE_FUNCTION
    WorkTask(int id_,
	     mesh_type &mesh_,
	     wparam_type wparam_)
      : id(id_),
	mesh(mesh_),
	wparam(wparam_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type &member) {
      
      auto blocksize = mesh.blocksize;
      auto ghostsize = mesh.ghostsize;
      auto varsize = mesh.varsize;

       key = mesh.key_at(id);

       int gblocksize = blocksize+2*ghostsize;

       size_t mem_size = gblocksize*gblocksize*
	 gblocksize*varsize*sizeof(real_type);

	Kokkos::single(Kokkos::PerTeam(member), [&]() {

	    void* ptr_mem = (mesh.mpool).allocate(mem_size);
	    if (ptr_mem==nullptr)
	      Kokkos::abort("need to increase memory_pool capacity");

	    ghostview = view_type((real_type*)ptr_mem,gblocksize,
				  gblocksize,gblocksize,varsize);

	    index_neighbors = mesh.neighbors_of(key);

	  });

	member.team_barrier();
	int ncell_glayer = blocksize*blocksize*ghostsize;

	//mask to compute the position of the leaf in the parent oct
	int parent_mask[3] = {1,2,4};

	for (int idir=0; idir<3; idir++){
	  for (int iface=0; iface<2; iface++){

	    int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	    key_type key_neigh = mesh.key_at(id_neigh);

	    //same level
	    if (key_neigh.level == key.level) {

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  is[idir] += (1-iface)*(blocksize-ghostsize);

		  for (int ivar=0; ivar<varsize; ivar++){
		    
		    ghostview(id[0],id[1],id[2],ivar) = 
		      mesh.block_at(id_neigh).view(is[0],is[1],is[2],ivar);
		  
		  }

		});

	    //coarser neighbor
	    } else if (key_neigh.level == (key.level-1)){

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  //check position of the current leaf relative to coarse neighbor
		  int parent_per1 = (key.morton & parent_mask[(idir+1)%3]) >> (idir+1)%3;
		  int parent_per2 = (key.morton & parent_mask[(idir+2)%3]) >> (idir+2)%3;

		  is[idir] = (is[idir] + (1-iface)*ghostsize%2)/2;
		  is[(idir+1)%3] = parent_per1*blocksize/2 + is[(idir+1)%3]/2;
		  is[(idir+2)%3] = parent_per2*blocksize/2 + is[(idir+2)%3]/2;

		  is[idir] += (1-iface)*(blocksize-(ghostsize+ghostsize%2)/2);

		  for (int ivar=0; ivar<varsize; ivar++){

		    ghostview(id[0],id[1],id[2],ivar) = 
		      mesh.block_at(id_neigh).view(is[0],is[1],is[2],ivar);

		  }

		});

	      //fine neighbors
	    } else {

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  for (int ivar = 0; ivar<varsize; ivar++){
		    ghostview(id[0],id[1],id[2],ivar) = 0.;
		  }

		  for (int ichild=0; ichild<8; ichild++){

		    int is_child[3] = {0};

		    int ichild_para = (ichild & parent_mask[idir]) >> idir;
		    int ichild_per1 = (ichild & parent_mask[(idir+1)%3]) >> (idir+1)%3;
		    int ichild_per2 = (ichild & parent_mask[(idir+2)%3]) >> (idir+2)%3;

		    is_child[idir] = 2*is[idir] +  ichild_para;
		    is_child[(idir+1)%3] = 2*is[(idir+1)%3] +  ichild_per1;
		    is_child[(idir+2)%3] = 2*is[(idir+2)%3] +  ichild_per2;

		    int id_neigh_child_per1 = is_child[(idir+1)%3]/blocksize;
		    int id_neigh_child_per2 = is_child[(idir+2)%3]/blocksize;

		    int id_neigh_child = index_neighbors
		      [idir*2*4+iface*4+(id_neigh_child_per2<<1)+id_neigh_child_per1];

		    is_child[(idir+1)%3] -= id_neigh_child_per1*blocksize;
		    is_child[(idir+2)%3] -= id_neigh_child_per2*blocksize;

		    is_child[idir] += (1-iface)*(blocksize-2*ghostsize);

		    for (int ivar=0; ivar<varsize; ivar++){

		      ghostview(id[0],id[1],id[2],ivar) +=
			mesh.block_at(id_neigh_child).view(is_child[0],is_child[1],is_child[2],ivar);
		    }

		  }

		  for (int ivar=0; ivar<varsize; ivar++){
		    ghostview(id[0],id[1],id[2],ivar) /=8 ;
		  }

		});

	    }

	  }
	}

	int ncell = blocksize*blocksize*blocksize;

	Kokkos::parallel_for
	  (Kokkos::TeamVectorRange(member, ncell),
	   [&] (int index){

	    //delinearization of the 1D index
	    int k = index/(blocksize*blocksize);
	    int j = (index-k*blocksize*blocksize)/blocksize;
	    int i = (index-k*blocksize*blocksize-j*blocksize);

	    int kg = k + ghostsize;
	    int jg = j + ghostsize;
	    int ig = i + ghostsize;

	    for (int ivar=0; ivar<varsize; ivar++){ 
	      ghostview(ig,jg,kg,ivar) = mesh.block_at(id).view(i,j,k,ivar);
	    }

	  });

	int size = 1;
	for (int il=0; il<(int) key.level; il++) size *=2;

	Kokkos::Array<int,3> coord = key.get_coord();

	real_type dleaf = mesh.lbox/size;
	real_type dx = dleaf/blocksize;

	real_type dt = wparam.cfl*dx*dx/6/(fmax(wparam.kappa1,wparam.kappa2));

	auto refParam = mesh.refParam;

	Kokkos::parallel_for
	  (Kokkos::TeamVectorRange(member, ncell),
	   [&] (int index){

	    //delinearization of the 1D index
	    int k = index/(blocksize*blocksize);
	    int j = (index-k*blocksize*blocksize)/blocksize;
	    int i = (index-k*blocksize*blocksize-j*blocksize);

	    int kg = k + ghostsize;
	    int jg = j + ghostsize;
	    int ig = i + ghostsize;

	    real_type x = coord[0]*dleaf+(i+0.5)*dx;
	    real_type y = coord[1]*dleaf+(j+0.5)*dx;
	    real_type z = coord[2]*dleaf+(k+0.5)*dx;

	    real_type dxcloc = 0.;
	    if (fabs(x-refParam.xc)<fabs(x-(refParam.xc-mesh.lbox))){
	      dxcloc = fabs(x-refParam.xc);
	    } else {
	      dxcloc = fabs(x-(refParam.xc-mesh.lbox));
	    }

	    real_type kappa_loc = 0.;
	    if ( (dxcloc*dxcloc+
	    	  (y-refParam.yc)*(y-refParam.yc)+
	    	  (z-refParam.zc)*(z-refParam.zc))<
	    	 refParam.rc*refParam.rc){

	      kappa_loc = wparam.kappa2;

	    } else {

	       kappa_loc = wparam.kappa1;

	    }

	       if ((x==0.5*mesh.lbox+0.5*dx) &&
	    	   (y==0.5*mesh.lbox+0.5*dx) &&
	    	   (z==0.5*mesh.lbox+0.5*dx)) {

		 for (int ivar=0; ivar<varsize; ivar++){
		   mesh.block_at(id).view2(i,j,k,ivar) = 1000.;
		 }

	    } else {

		 for (int ivar=0; ivar<varsize; ivar++){
		   mesh.block_at(id).view2(i,j,k,ivar) = ghostview(ig,jg,kg,ivar) +
		     dt/(dx*dx)*kappa_loc*
		     (- 6*ghostview(ig,jg,kg,ivar)
		      + ghostview(ig-1,jg,kg,ivar) + ghostview(ig+1,jg,kg,ivar)
		      + ghostview(ig,jg-1,kg,ivar) + ghostview(ig,jg+1,kg,ivar)
		      + ghostview(ig,jg,kg-1,ivar) + ghostview(ig,jg,kg+1,ivar));

		 }
	    }

	  });

	Kokkos::single(Kokkos::PerTeam(member), [&]() {
	    (mesh.mpool).deallocate(ghostview.data(),mem_size);
	  });

    }

  };

  // ************************************************************************
  // Launch WorkTask recursively on all blocks
  struct WorkStep {
    using value_type = void;
    using wparam_type = Param;

    key_type key;
    mesh_type mesh;
    wparam_type wparam;

    KOKKOS_INLINE_FUNCTION
    WorkStep(key_type key_, mesh_type &mesh_, wparam_type wparam_)
      : key(key_), mesh(mesh_), wparam(wparam_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type& member) {
      auto& scheduler = member.scheduler();

      Kokkos::single(Kokkos::PerTeam(member), [&]() {

	  int id = mesh.index_at(key);

	  if (mesh.leaf_at(id)){

	    Kokkos::task_spawn
	      (Kokkos::TaskTeam(scheduler, Kokkos::TaskPriority::High),
	       WorkKernel::WorkTask{id, mesh, wparam});

	  } else {

	    // Kokkos::parallel_for
	    //   (Kokkos::TeamVectorRange(member, 8),
	    //    [&] (int ichild){

		for (int ichild = 0; ichild<8; ichild++){

	      key_type key_child =
		key_type((key.morton<<3) + ichild, key.level+1);

	      //spawn recursively WorkStep on child leaves
	      Kokkos::task_spawn(Kokkos::TaskTeam(scheduler),
	      			 WorkKernel::WorkStep{key_child,mesh, wparam});

	      }

	  }

	  });

    }

  };

}

