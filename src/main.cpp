/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#include <Kokkos_Core.hpp>
#include <amr/AmrCore.hpp>
#include <amr/AmrKernel.hpp>
#include <hd/HdKernel.hpp>
#include <io/VtuFile.hpp>

 // ************************************************************************
  // struct for the AMR scheduler parameter for tasks
  struct SchedParam{

    size_t MinBlockSize;
    size_t MaxBlockSize;
    size_t SuperBlockSize;
    size_t TotalMemory;
    int reset_frequency;

    SchedParam(IO::IniFile iparam)
      : MinBlockSize(), MaxBlockSize(),
	SuperBlockSize(), TotalMemory(),
	reset_frequency()
    {

      int MaxBlock_over_Kernel = 
	iparam.get("scheduler","MaxBlock_over_Kernel",3);
      int SuperBlock_over_MaxBlock = 
	iparam.get("scheduler","SuperBlock_over_MaxBlock",100);
      int MemoryPerStep_over_MaxBlock = 
	iparam.get("scheduler","MemoryPerStep_over_MaxBlock",10000);

      reset_frequency = iparam.get("scheduler","reset_frequency",100);

      MinBlockSize    = sizeof(real_type);
      MaxBlockSize    = MaxBlock_over_Kernel*sizeof(HdKernel::HdTask);
      SuperBlockSize  = SuperBlock_over_MaxBlock*MaxBlockSize;
      TotalMemory     = MemoryPerStep_over_MaxBlock*MaxBlockSize*reset_frequency;
      
    }
  };

// ************************************************************************
int main(int argc, char* argv[])
{
  using hdparam_type = HdKernel::Param;
  using sparam_type = SchedParam;

  Kokkos::initialize(argc, argv);
  {
    std::cout << "##########################\n";
    std::cout << "Kokkos configuration      \n";
    std::cout << "##########################\n";

    std::ostringstream msg;
    if (Kokkos::hwloc::available())
      {
	msg << "hwloc( NUMA[" << Kokkos::hwloc::get_available_numa_count()
	    << "] x CORE["    << Kokkos::hwloc::get_available_cores_per_numa()
	    << "] x HT["      << Kokkos::hwloc::get_available_threads_per_core()
	    << "] )"
	    << std::endl ;
      }
    Kokkos::print_configuration(msg);
    std::cout << msg.str();
    std::cout << "##########################\n";

    //Read parameters
    if (argc <2) Kokkos::abort("Need to provide a .ini file \n");
    IO::IniFile iparam(argv[1]);

    //create mesh
    auto mesh = mesh_type(iparam);
   
    //hydro param
    auto hdparam = hdparam_type(iparam);

    //scheduler param
    auto sparam = sparam_type(iparam);

    //create scheduler
    auto mpools = memory_pool(memory_space(),
			      sparam.TotalMemory,
			      sparam.MinBlockSize,
			      sparam.MaxBlockSize,
			      sparam.SuperBlockSize);

    auto scheduler = scheduler_type(mpools);
    
    //uniform grid at levelmin
    int level_root = 0; int morton_root = 0;
    key_type key_root = key_type(morton_root,level_root);

    Kokkos::host_spawn(Kokkos::TaskTeam(scheduler),
		       AmrKernel::UniformGridStep{key_root,mesh});

    Kokkos::wait(scheduler);
    Kokkos::fence();

    //refinement of the uniform initial condition
    for (int ir=0;ir<3*((int) mesh.levelmax-(int) mesh.levelmin);ir++){

      Kokkos::host_spawn(Kokkos::TaskTeam(scheduler),
    			 AmrKernel::AmrCycleStep{key_root,mesh});

      Kokkos::wait(scheduler);
      Kokkos::fence();

      Kokkos::parallel_for(mesh.capacity(), KOKKOS_LAMBDA (int ibt) {

    	  if (mesh.valid_at(ibt)){
    	    block_type &block = mesh.block_at(ibt);
    	    block.f_AmrMark.clear();
	    block.f_AmrAdapt.clear();
    	  }

    	});

    }

    Kokkos::fence();

    //time loop
    int nleaf = 0; real_type total_time = 0.;
    mesh.init=false;
    
    for (int it = 0; it<mesh.nt;it++){

      if((it%sparam.reset_frequency)==0) {
	
      	scheduler = scheduler_type(memory_space(),
      				   sparam.TotalMemory,
      				   sparam.MinBlockSize,
      				   sparam.MaxBlockSize,
      				   sparam.SuperBlockSize);
      }

      Kokkos::Timer timer;

      //schedule first task on device AmrCycle
      Kokkos::host_spawn(Kokkos::TaskTeam(scheduler),
      			 AmrKernel::AmrCycleStep{key_root,mesh}
      			 );

      //wait on all tasks submitted to the scheduler to be done
      Kokkos::wait(scheduler);
      Kokkos::fence();
      
      //mesh.mpool.print_state(std::cout);
    
      if(hdparam.task_para) {

	//schedule first task on device WorkKernel
	Kokkos::host_spawn(Kokkos::TaskTeam(scheduler),
			   HdKernel::HdStep{key_root,mesh,hdparam}
			   );

	//wait on all tasks submitted to the scheduler to be done
	Kokkos::wait(scheduler);
	Kokkos::fence();

      } else {
 
	int gblocksize = mesh.blocksize+2*mesh.ghostsize;

	size_t mem_size = gblocksize*gblocksize*
	  gblocksize*mesh.varsize*sizeof(real_type);
       
	//data-parallel Hd kernel
	Kokkos::parallel_for
	  (Kokkos::TeamPolicy<>
	   (mesh.capacity(), Kokkos::AUTO).set_scratch_size
	   (1, Kokkos::PerTeam(mem_size),Kokkos::PerThread(0)),
	   KOKKOS_LAMBDA( const Kokkos::TeamPolicy<>::member_type& member){

	    int id = member.league_rank();
	    if (mesh.leaf_at(id)){
	  
	      HdKernel::HdData kernel{id,mesh};
	      kernel(member,hdparam);
	  
	    }
	  });

      }

      //data-parallel operations
      mesh.blockmap.begin_erase();
      auto blocksize = mesh.blocksize;

      Kokkos::parallel_for
    	(Kokkos::TeamPolicy<>( mesh.capacity(), Kokkos::AUTO),
    	 KOKKOS_LAMBDA( const Kokkos::TeamPolicy<>::member_type& member){

    	  int id = member.league_rank();

    	  if (mesh.valid_at(id)){
    	    block_type &block = mesh.block_at(id);

    	    //clean futures
    	    Kokkos::single(Kokkos::PerTeam(member), [&]() {
    		block.f_AmrMark.clear();
    		block.f_AmrAdapt.clear();
    	      });

    	    if(!mesh.leaf_at(id)){

    	      //clean key to erase in blockmap
    	      key_type key = mesh.key_at(id);
    	      key_type key_parent = key_type((key.morton>>3),key.level-1);

    	      if (mesh.leaf_at(key_parent)){

    		Kokkos::single(Kokkos::PerTeam(member), [&]() {
    		    auto result=mesh.blockmap.erase(key);
    		  });

    	      }
    	    }
    	  }

    	  //copy view2 in view
    	  if (mesh.leaf_at(id)){
    	    block_type &block = mesh.block_at(id);

    	    int ncell = blocksize*blocksize*blocksize;
    	    Kokkos::parallel_for
    	      (Kokkos::TeamVectorRange(member, ncell),
    	       [&](int index) {

    		int kcell = index/(blocksize*blocksize);
    		int jcell = (index-kcell*blocksize*blocksize)/blocksize;
    		int icell = (index-kcell*blocksize*blocksize-jcell*blocksize);

    		for (int ivar=0; ivar<mesh.varsize; ivar++){
    		  block.view(icell,jcell,kcell,ivar) = 
    		    block.view2(icell,jcell,kcell,ivar);
    		}

    	      });

    	  }

    	});

      Kokkos::fence();
      mesh.blockmap.end_erase();

      //timestep reduction
      Kokkos::parallel_reduce
    	(Kokkos::TeamPolicy<>( mesh.capacity(), Kokkos::AUTO),
    	 KOKKOS_LAMBDA( const Kokkos::TeamPolicy<>::member_type& member, real_type &dt_leaf){

    	  int id = member.league_rank();
	 
    	  if (mesh.leaf_at(id)){
    	    block_type &block = mesh.block_at(id);

    	    int size = 1;
    	    for (int il=0; il<(int) mesh.key_at(id).level; il++) size *=2;

    	    real_type dleaf = mesh.lbox/size;
    	    real_type dx = dleaf/mesh.blocksize;

    	    int ncell = mesh.blocksize*mesh.blocksize*mesh.blocksize;
    	    Kokkos::parallel_reduce
    	      (Kokkos::TeamVectorRange(member, ncell),
    	       [&] (int index, real_type &dt_cell){

    		//delinearization of the 1D index
    		int kcell = index/(blocksize*blocksize);
    		int jcell = (index-kcell*blocksize*blocksize)/blocksize;
    		int icell = (index-kcell*blocksize*blocksize-jcell*blocksize);

    		real_type rhog = block.view(icell,jcell,kcell,ID);
    		real_type ug   = block.view(icell,jcell,kcell,IU)/rhog;
    		real_type vg   = block.view(icell,jcell,kcell,IV)/rhog;
    		real_type wg   = block.view(icell,jcell,kcell,IW)/rhog;
    		real_type ekg  = 0.5*rhog*(ug*ug+vg*vg+wg*wg);
    		real_type pg   = (block.view(icell,jcell,kcell,IE)-ekg)*(hdparam.gamma-1.);
    		real_type cg   = sqrt(hdparam.gamma*pg/rhog);

		real_type dt_dir = hdparam.cfl*dx/(cg+fabs(ug));
    		dt_cell = dt_dir < dt_cell ? dt_dir : dt_cell;

		dt_dir = hdparam.cfl*dx/(cg+fabs(vg));
    		dt_cell = dt_dir < dt_cell ? dt_dir : dt_cell;

		dt_dir = hdparam.cfl*dx/(cg+fabs(wg));
    		dt_cell = dt_dir < dt_cell ? dt_dir : dt_cell;
		 
    	      },Kokkos::Min<real_type>(dt_leaf));

    	  }

    	},Kokkos::Min<real_type>(mesh.dt));

      nleaf = 0;
      //number of leaf reduction
      Kokkos::parallel_reduce
    	(Kokkos::TeamPolicy<>( mesh.capacity(), Kokkos::AUTO),
    	 KOKKOS_LAMBDA( const Kokkos::TeamPolicy<>::member_type& member, int &inner_nleaf){

    	  int id = member.league_rank();
	 
	  Kokkos::single(Kokkos::PerTeam(member), [&]() {
	      
	      if (mesh.leaf_at(id)){
		inner_nleaf += 1; 	
	      }

	    });

    	},Kokkos::Sum<int>(nleaf));

      auto time = timer.seconds();
      std::printf("Time: %e s, iteration: %i \n",time,it);
      std::printf("Mcell-update/s: %g \n",
		1.*nleaf*mesh.blocksize*mesh.blocksize*mesh.blocksize/time/1E6);
      total_time += time;

      
    }


    Kokkos::Timer timer_output;

    IO::VtuFile output("output.vtu");
    output.open();
    output.write_header(nleaf,mesh.blocksize);
    std::printf("write points\n");
    output.write_points(mesh);
    std::printf("write cells\n");
    output.write_cells(nleaf,mesh.blocksize);
    std::printf("write celldata\n");
    output.write_cellData(mesh);
    output.write_footer();
    output.close();

    auto time_output = timer_output.seconds();
    std::printf("Time for output: %e s \n",time_output);

    // Print the timing results
    std::printf("Total time: %g s \n", total_time);
    std::printf("Number of leaf: %i \n", nleaf);
    
  }

  Kokkos::finalize();
  return 0;
}
