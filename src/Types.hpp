/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <Kokkos_Core.hpp>

using real_type   = double;
using morton_type = uint32_t;
using level_type  = uint32_t; //because of padding in Amr::Key could be optimized

using scheduler_type = Kokkos::TaskSchedulerMultiple<Kokkos::DefaultExecutionSpace>;
using device_type    = scheduler_type::execution_space::device_type;
using member_type    = scheduler_type::member_type;
using memory_space   = scheduler_type::memory_space;
using memory_pool    = scheduler_type::memory_pool;

using future_type     = Kokkos::BasicFuture<void,scheduler_type>;
using future_int_type = Kokkos::BasicFuture<int, scheduler_type>;

using view_type = Kokkos::View<real_type****,Kokkos::LayoutLeft,
				    device_type,Kokkos::MemoryUnmanaged>;


enum Ivar{

  ID = 0,
  IU = 1,
  IV = 2,
  IW = 3,
  IE = 4

};
