/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <inih/INIReader.hpp>
#include <Types.hpp>

// ************************************************************************
// Inputs and outputs
namespace IO {

  // ************************************************************************
  // ini file for input params
  struct IniFile {

    std::string filename;
    INIReader map;

    IniFile(std::string filename_)
    :filename(filename_),map(filename_)
    {

      if (map.ParseError() < 0)
	Kokkos::abort("error loading ini file \n");

    };

    int get(const std::string& section,
	    const std::string& name,
	    int default_value)
    {

      return map.GetInteger(section,name,default_value);

    };

    std::string get(const std::string& section,
		    const std::string& name,
		    const std::string& default_value)
    {

      return map.Get(section,name,default_value);

    };

    bool get(const std::string& section,
	     const std::string& name,
	     bool default_value)
    {

      return map.GetBoolean(section,name,default_value);

    };

    real_type get(const std::string& section,
		  const std::string& name,
		  real_type default_value)
    {

      return map.GetReal(section,name,default_value);

    };

  };

}
