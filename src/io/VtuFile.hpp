/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <fstream>
#include <amr/AmrCore.hpp>

namespace IO {
  // ************************************************************************
  // vtu file for outputs
  struct VtuFile {

    std::string filename;
    std::ofstream os;


     VtuFile(std::string filename_)
      :filename(filename_),os()
    {}

    void open()
    {
      os.open(filename.c_str());
    }

    void write_header(int nleaf, int blocksize)
    {

      int ncell = blocksize*blocksize*blocksize;
      int nvertex = (blocksize+1)*(blocksize+1)*(blocksize+1);

      os << "<VTKFile type=\"UnstructuredGrid\" version=\"1.0\">\n";
      os << "<UnstructuredGrid>\n";
      os << "<Piece NumberOfPoints=\"" << nleaf*nvertex << "\" NumberOfCells=\""
	 << nleaf*ncell << "\">\n";
    }

    void write_points(mesh_type mesh)
    {
      os << "<Points>\n";
      os << "<DataArray type=\"Float64\" NumberOfComponents=\"3\" "
        "format=\"ascii\">\n";

      int blocksize = mesh.blocksize;
      int nvertex = (blocksize+1)*(blocksize+1)*(blocksize+1);

      for (int id =0; id<mesh.capacity(); id++) {

	if (mesh.leaf_at(id)) {

	  key_type key = mesh.key_at(id);
	  Kokkos::Array<int,3> coord = key.get_coord();

	  int size = 1;
	  for (int il=0; il<(int) key.level; il++) size *=2;

	  for (int index=0; index<nvertex; index++){

	    int kv = index/((blocksize+1)*(blocksize+1));
	    int jv = (index-kv*(blocksize+1)*(blocksize+1))/(blocksize+1);
	    int iv = (index-kv*(blocksize+1)*(blocksize+1)-jv*(blocksize+1));

	    os << (double) mesh.lbox*(coord[0]+1.*iv/blocksize)/size << " ";
	    os << (double) mesh.lbox*(coord[1]+1.*jv/blocksize)/size << " ";
	    os << (double) mesh.lbox*(coord[2]+1.*kv/blocksize)/size << "\n";

	  }

	}

      }

      os << "</DataArray>\n";
      os << "</Points>\n";

    }

    void write_cells(int nleaf, int blocksize)
    {

      int ncell   = blocksize*blocksize*blocksize;
      int nvertex = (blocksize+1)*(blocksize+1)*(blocksize+1);

      int vertex_order[8] = {0, 1, 3, 2, 4, 5, 7, 6};

      os << "<Cells>\n";
      os << "<DataArray type=\"Int64\" Name=\"connectivity\" "
        "format=\"ascii\">\n";

      for (int ileaf =0; ileaf<nleaf; ileaf++) {

	for (int icell = 0; icell<blocksize; icell++) {
	  for (int jcell = 0; jcell<blocksize; jcell++) {
	    for (int kcell = 0; kcell<blocksize; kcell++) {

	      for (int ivertex = 0; ivertex<8; ivertex++){

		int ivx = (vertex_order[ivertex] & 1);
		int ivy = (vertex_order[ivertex] & 2) >> 1;
		int ivz = (vertex_order[ivertex] & 4) >> 2;

		ivx += icell;
		ivy += jcell;
		ivz += kcell;

		uint64_t index_vertex = ileaf*nvertex + ivx +
		  ivy*(blocksize+1) + ivz*(blocksize+1)*(blocksize+1);

		os << index_vertex << " ";

	      }
	      os << "\n";

	    }
	  }
	}

      }

      os << "</DataArray>\n";

      int cell_type =  12;

      os << "<DataArray type=\"Int8\" Name=\"types\" format=\"ascii\" "
        "RangeMin=\""
	 << cell_type << "\" RangeMax=\"" << cell_type << "\">\n";

      for (int ileaf = 0; ileaf < nleaf*ncell; ileaf++) {
	os << cell_type << "\n";
      }
      os << "</DataArray>\n";


      os << "<DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\" "
        "RangeMin=\""
  	 << 8 << "\" RangeMax=\"" << ncell* nleaf * 8 << "\">\n";

      int acc = 8;
      for (int ileaf = 0; ileaf < nleaf*ncell; ileaf++) {
  	os << acc << "\n";
  	acc += 8;
      }

      os << "</DataArray>\n";

      os << "</Cells>\n";
    }

    void write_cellData(mesh_type mesh)
    {
      os << "<CellData Scalars=\"level\">\n";
      os << "<DataArray type=\"Int8\" Name=\"level\" "
	"format=\"ascii\">\n";

      for (int id =0; id<mesh.capacity(); id++) {

	if (mesh.leaf_at(id)) {

	  key_type key = mesh.key_at(id);

	  for (int icell=0; icell<mesh.blocksize; icell++){
	    for (int jcell=0; jcell<mesh.blocksize; jcell++){
	      for (int kcell=0; kcell<mesh.blocksize; kcell++){

		os << (int) key.level << "\n";

	      }
	    }
	  }

	}

      }

      os << "</DataArray>\n";

       os << "<DataArray type=\"Float64\" Name=\"temp\" "
	"format=\"ascii\">\n";

      for (int id =0; id<mesh.capacity(); id++) {

	if (mesh.leaf_at(id)) {

	  block_type &block = mesh.block_at(id);

	  for (int icell=0; icell<mesh.blocksize; icell++){
	    for (int jcell=0; jcell<mesh.blocksize; jcell++){
	      for (int kcell=0; kcell<mesh.blocksize; kcell++){

		os <<  block.view2(icell,jcell,kcell,0) << "\n";

	      }
	    }
	  }

	}

      }

      os << "</DataArray>\n";

      os << "</CellData>\n";
    }

    void write_footer()
    {
      os << "</Piece>\n";
      os << "</UnstructuredGrid>\n";
      os << "</VTKFile>\n";
    }

    void close()
    {
      os.close();
    }

  };

}
