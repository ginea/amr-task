/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <Kokkos_Core.hpp>
#include <Kokkos_UnorderedMap.hpp>
#include <Kokkos_Array.hpp>
#include <amr/AmrCore.hpp>
#include <io/IniFile.hpp>

// ************************************************************************
// Namespace for an all-regime hydro kernel
namespace HdKernel {

  struct Param {

    real_type gamma;
    real_type cfl;
    int task_para;

    Param(IO::IniFile iparam)
      : cfl()
    {
      gamma     = iparam.get("hd","gamma",1.66667);
      cfl       = iparam.get("hd","cfl",0.9);
      task_para = iparam.get("hd","task_para",0); 
    }

  };

  // ************************************************************************
  // Kernel task to perform hydro on a leaf
  struct HdTask {
    using value_type = void;
    using hdparam_type = Param;

    int id;
    mesh_type mesh;

    view_type ghostview;
    hdparam_type hdparam;

    KOKKOS_INLINE_FUNCTION
    HdTask(int id_,
	   mesh_type mesh_,
	   hdparam_type hdparam_)
      : id(id_),
	mesh(mesh_),
	hdparam(hdparam_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type &member) {
   
      auto blocksize = mesh.blocksize;
      auto ghostsize = mesh.ghostsize;
      auto varsize = mesh.varsize;

       key_type key = mesh.key_at(id);

       int gblocksize = blocksize+2*ghostsize;

       size_t mem_size = gblocksize*gblocksize*
	 gblocksize*varsize*sizeof(real_type);

	Kokkos::single(Kokkos::PerTeam(member), [&]() {

	    void* ptr_mem = (mesh.mpool).allocate(mem_size);
	    if (ptr_mem==nullptr)
	      Kokkos::abort("need to increase memory_pool capacity");

	    ghostview = view_type((real_type*)ptr_mem,gblocksize,
				  gblocksize,gblocksize,varsize);

	  });
       

	Kokkos::Array<int,24>  index_neighbors = mesh.neighbors_of(key);
	member.team_barrier();
	int ncellnvar_glayer = blocksize*blocksize*ghostsize*varsize;
	int ncell_glayer = blocksize*blocksize*ghostsize;

	//mask to compute the position of the leaf in the parent oct
	int parent_mask[3] = {1,2,4};

	for (int idir=0; idir<3; idir++){
	  for (int iface=0; iface<2; iface++){

	    int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	    key_type key_neigh = mesh.key_at(id_neigh);

	    //same level
	    if (key_neigh.level == key.level) {

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncellnvar_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  int ivar = index/(blocksize*blocksize*ghostsize);
		  is[idir] = (index-ivar*blocksize*blocksize*ghostsize)/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-ivar*blocksize*blocksize*ghostsize-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-ivar*blocksize*blocksize*ghostsize-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  is[idir] += (1-iface)*(blocksize-ghostsize);

		 
		  
		    ghostview(id[0],id[1],id[2],ivar) = 
		      mesh.block_at(id_neigh).view(is[0],is[1],is[2],ivar);
		         
		 

		});

	    //coarser neighbor
	    } else if (key_neigh.level == (key.level-1)){

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  //check position of the current leaf relative to coarse neighbor
		  int parent_per1 = (key.morton & parent_mask[(idir+1)%3]) >> (idir+1)%3;
		  int parent_per2 = (key.morton & parent_mask[(idir+2)%3]) >> (idir+2)%3;

		  is[idir] = (is[idir] + (1-iface)*ghostsize%2)/2;
		  is[(idir+1)%3] = parent_per1*blocksize/2 + is[(idir+1)%3]/2;
		  is[(idir+2)%3] = parent_per2*blocksize/2 + is[(idir+2)%3]/2;

		  is[idir] += (1-iface)*(blocksize-(ghostsize+ghostsize%2)/2);

		  for (int ivar=0; ivar<varsize; ivar++){

		    ghostview(id[0],id[1],id[2],ivar) = 
		      mesh.block_at(id_neigh).view(is[0],is[1],is[2],ivar);

		  }

		});

	      //fine neighbors
	    } else {

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  for (int ivar = 0; ivar<varsize; ivar++){
		    ghostview(id[0],id[1],id[2],ivar) = 0.;
		  }

		  for (int ichild=0; ichild<8; ichild++){

		    int is_child[3] = {0};

		    int ichild_para = (ichild & parent_mask[idir]) >> idir;
		    int ichild_per1 = (ichild & parent_mask[(idir+1)%3]) >> (idir+1)%3;
		    int ichild_per2 = (ichild & parent_mask[(idir+2)%3]) >> (idir+2)%3;

		    is_child[idir] = 2*is[idir] +  ichild_para;
		    is_child[(idir+1)%3] = 2*is[(idir+1)%3] +  ichild_per1;
		    is_child[(idir+2)%3] = 2*is[(idir+2)%3] +  ichild_per2;

		    int id_neigh_child_per1 = is_child[(idir+1)%3]/blocksize;
		    int id_neigh_child_per2 = is_child[(idir+2)%3]/blocksize;

		    int id_neigh_child = index_neighbors
		      [idir*2*4+iface*4+(id_neigh_child_per2<<1)+id_neigh_child_per1];

		    is_child[(idir+1)%3] -= id_neigh_child_per1*blocksize;
		    is_child[(idir+2)%3] -= id_neigh_child_per2*blocksize;

		    is_child[idir] += (1-iface)*(blocksize-2*ghostsize);

		    for (int ivar=0; ivar<varsize; ivar++){

		      ghostview(id[0],id[1],id[2],ivar) +=
			mesh.block_at(id_neigh_child).view(is_child[0],is_child[1],is_child[2],ivar);
		    }

		  }

		  for (int ivar=0; ivar<varsize; ivar++){
		    ghostview(id[0],id[1],id[2],ivar) /=8 ;
		  }

		});

	    }

	  }
	}

	int ncell = blocksize*blocksize*blocksize;
	int ncellnvar = blocksize*blocksize*blocksize*varsize;

	Kokkos::parallel_for
	  (Kokkos::TeamVectorRange(member, ncellnvar),
	   [&] (int index){

	    //delinearization of the 1D index
	    int ivar = index/(blocksize*blocksize*blocksize);
	    int k = (index-ivar*blocksize*blocksize*blocksize)/(blocksize*blocksize);
	    int j = (index-ivar*blocksize*blocksize*blocksize-k*blocksize*blocksize)/blocksize;
	    int i = (index-ivar*blocksize*blocksize*blocksize-k*blocksize*blocksize-j*blocksize);

	    int kg = k + ghostsize;
	    int jg = j + ghostsize;
	    int ig = i + ghostsize;

	    ghostview(ig,jg,kg,ivar) = mesh.block_at(id).view(i,j,k,ivar);

	  });

	int size = 1;
	for (int il=0; il<(int) key.level; il++) size *=2;

	real_type dleaf = mesh.lbox/size;
	real_type dx = dleaf/blocksize;

	real_type dt = mesh.dt;

	int ilr[3] = {1,0,0}; int jlr[3] = {0,1,0}; int klr[3] = {0,0,1};

	Kokkos::parallel_for
	  (Kokkos::TeamVectorRange(member, ncell),
	   [&] (int index){

	    //delinearization of the 1D index
	    int k = index/(blocksize*blocksize);
	    int j = (index-k*blocksize*blocksize)/blocksize;
	    int i = (index-k*blocksize*blocksize-j*blocksize);

	    int ig = i + ghostsize;
	    int jg = j + ghostsize;
	    int kg = k + ghostsize;

	    Kokkos::Array<real_type,5> Ug;
	    Kokkos::Array<real_type,5> Un;

	    for (int ivar=0; ivar<varsize; ivar++){

	      Ug[ivar] = ghostview(ig,jg,kg,ivar);

	    }

	    real_type rhog = Ug[ID];
	    real_type ug[3] = {Ug[IU]/rhog,
			       Ug[IV]/rhog,
			       Ug[IW]/rhog};
	    real_type ekg  = 0.5*rhog*(ug[0]*ug[0]+ug[1]*ug[1]+ug[2]*ug[2]);
	    real_type pg   = (Ug[IE]-ekg)*(hdparam.gamma-1.);
	    real_type ag   = rhog*sqrt(hdparam.gamma*pg/rhog);

	    for (int idir=0; idir<3; idir++){
	      for (int iface=0;iface<2;iface++){
		
	    	int kn = kg + (2*iface-1)*klr[idir];
	    	int jn = jg + (2*iface-1)*jlr[idir];
	    	int in = ig + (2*iface-1)*ilr[idir];

		for (int ivar=0; ivar<varsize; ivar++){

		  Un[ivar] = ghostview(in,jn,kn,ivar);

		}

	    	real_type rhon = Un[ID];
	    	real_type un[3] = {Un[IU]/rhon,
	    			   Un[IV]/rhon,
	    			   Un[IW]/rhon};
	    	real_type ekn  = 0.5*rhon*(un[0]*un[0]+un[1]*un[1]+un[2]*un[2]);
	    	real_type pn   = (Un[IE]-ekn)*(hdparam.gamma-1.);
	    	real_type an   = rhon*sqrt(hdparam.gamma*pn/rhon);
		  
	    	real_type ustar = 0.; real_type pstar = 0.;
	    	real_type aface = 1.1*fmax(ag,an);
	    	if(iface==1) {
	    	  ustar = 0.5*(ug[idir]+un[idir])-0.5*(pn-pg)/aface;
	    	  pstar = 0.5*(pg+pn)-0.5*(un[idir]-ug[idir])*aface;
	    	} else {
	    	  ustar = 0.5*(ug[idir]+un[idir])-0.5*(pg-pn)/aface;
	    	  pstar = 0.5*(pg+pn)-0.5*(ug[idir]-un[idir])*aface;
	    	}
		
	    	Kokkos::Array<real_type,5> flux;

	    	if((ustar>0. && iface==1)||(ustar<0. && iface==0)) {
	    	  flux[ID] = Ug[ID]*ustar;
	    	  flux[IU] = Ug[IU]*ustar;
	    	  flux[IV] = Ug[IV]*ustar;
	    	  flux[IW] = Ug[IW]*ustar;
	    	  flux[IE] = Ug[IE]*ustar;
 	    	} else {
	    	  flux[ID] = Un[ID]*ustar;
	    	  flux[IU] = Un[IU]*ustar;
	    	  flux[IV] = Un[IV]*ustar;
	    	  flux[IW] = Un[IW]*ustar;
	    	  flux[IE] = Un[IE]*ustar;
	    	}
		
	    	flux[IU+idir] += pstar;
	    	flux[IE] += pstar*ustar;

		 for (int ivar=0; ivar<varsize; ivar++){
		   mesh.block_at(id).view2(i,j,k,ivar) -= (2*iface-1)*dt/dx*flux[ivar];
		  
		 }

	      }
	    }

	  });

	Kokkos::single(Kokkos::PerTeam(member), [&]() {
	    (mesh.mpool).deallocate(ghostview.data(),mem_size);
	  });

    }

  };


  // ************************************************************************
  //Data-parallel Kernel to perform hydro on a leaf
  struct HdData {
    using hdparam_type = Param;

    int id;
    mesh_type mesh;

    KOKKOS_INLINE_FUNCTION
    HdData(int id_,
	   mesh_type mesh_
	   )
      : id(id_),
	mesh(mesh_)
	
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(const Kokkos::TeamPolicy<>::member_type& member,
		    const hdparam_type hdparam){

      auto blocksize = mesh.blocksize;
      auto ghostsize = mesh.ghostsize;
      auto varsize = mesh.varsize;

       key_type key = mesh.key_at(id);

       int gblocksize = blocksize+2*ghostsize;
       
       Kokkos::View<real_type****,device_type> ghostview(member.team_scratch(1),gblocksize,gblocksize,gblocksize,varsize);

	Kokkos::Array<int,24>  index_neighbors = mesh.neighbors_of(key);
	member.team_barrier();
	int ncellnvar_glayer = blocksize*blocksize*ghostsize*varsize;
	int ncell_glayer = blocksize*blocksize*ghostsize;

	//mask to compute the position of the leaf in the parent oct
	int parent_mask[3] = {1,2,4};

	for (int idir=0; idir<3; idir++){
	  for (int iface=0; iface<2; iface++){

	    int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	    key_type key_neigh = mesh.key_at(id_neigh);

	    //same level
	    if (key_neigh.level == key.level) {

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncellnvar_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  int ivar = index/(blocksize*blocksize*ghostsize);
		  is[idir] = (index-ivar*blocksize*blocksize*ghostsize)/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-ivar*blocksize*blocksize*ghostsize-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-ivar*blocksize*blocksize*ghostsize-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  is[idir] += (1-iface)*(blocksize-ghostsize);

		 
		  
		    ghostview(id[0],id[1],id[2],ivar) = 
		      mesh.block_at(id_neigh).view(is[0],is[1],is[2],ivar);
		         
		 

		});

	    //coarser neighbor
	    } else if (key_neigh.level == (key.level-1)){

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  //check position of the current leaf relative to coarse neighbor
		  int parent_per1 = (key.morton & parent_mask[(idir+1)%3]) >> (idir+1)%3;
		  int parent_per2 = (key.morton & parent_mask[(idir+2)%3]) >> (idir+2)%3;

		  is[idir] = (is[idir] + (1-iface)*ghostsize%2)/2;
		  is[(idir+1)%3] = parent_per1*blocksize/2 + is[(idir+1)%3]/2;
		  is[(idir+2)%3] = parent_per2*blocksize/2 + is[(idir+2)%3]/2;

		  is[idir] += (1-iface)*(blocksize-(ghostsize+ghostsize%2)/2);

		  for (int ivar=0; ivar<varsize; ivar++){

		    ghostview(id[0],id[1],id[2],ivar) = 
		      mesh.block_at(id_neigh).view(is[0],is[1],is[2],ivar);

		  }

		});

	      //fine neighbors
	    } else {

	      //fill ghost layers (without corners)
	      Kokkos::parallel_for
		(Kokkos::TeamVectorRange(member, ncell_glayer),
		 [&] (int index){

		  int is[3] = {0}; int id[3] = {0};

		  is[idir] = index/(blocksize*blocksize);
		  is[(idir+1)%3] = (index-is[idir]*blocksize*blocksize)/blocksize;
		  is[(idir+2)%3] = index-is[idir]*blocksize*blocksize-is[(idir+1)%3]*blocksize;

		  id[idir] = ghostsize + is[idir] + iface*blocksize-(1-iface)*ghostsize;
		  id[(idir+1)%3] = ghostsize + is[(idir+1)%3];
		  id[(idir+2)%3] = ghostsize + is[(idir+2)%3];

		  for (int ivar = 0; ivar<varsize; ivar++){
		    ghostview(id[0],id[1],id[2],ivar) = 0.;
		  }

		  for (int ichild=0; ichild<8; ichild++){

		    int is_child[3] = {0};

		    int ichild_para = (ichild & parent_mask[idir]) >> idir;
		    int ichild_per1 = (ichild & parent_mask[(idir+1)%3]) >> (idir+1)%3;
		    int ichild_per2 = (ichild & parent_mask[(idir+2)%3]) >> (idir+2)%3;

		    is_child[idir] = 2*is[idir] +  ichild_para;
		    is_child[(idir+1)%3] = 2*is[(idir+1)%3] +  ichild_per1;
		    is_child[(idir+2)%3] = 2*is[(idir+2)%3] +  ichild_per2;

		    int id_neigh_child_per1 = is_child[(idir+1)%3]/blocksize;
		    int id_neigh_child_per2 = is_child[(idir+2)%3]/blocksize;

		    int id_neigh_child = index_neighbors
		      [idir*2*4+iface*4+(id_neigh_child_per2<<1)+id_neigh_child_per1];

		    is_child[(idir+1)%3] -= id_neigh_child_per1*blocksize;
		    is_child[(idir+2)%3] -= id_neigh_child_per2*blocksize;

		    is_child[idir] += (1-iface)*(blocksize-2*ghostsize);

		    for (int ivar=0; ivar<varsize; ivar++){

		      ghostview(id[0],id[1],id[2],ivar) +=
			mesh.block_at(id_neigh_child).view(is_child[0],is_child[1],is_child[2],ivar);
		    }

		  }

		  for (int ivar=0; ivar<varsize; ivar++){
		    ghostview(id[0],id[1],id[2],ivar) /=8 ;
		  }

		});

	    }

	  }
	}

	int ncell = blocksize*blocksize*blocksize;
	int ncellnvar = blocksize*blocksize*blocksize*varsize;

	Kokkos::parallel_for
	  (Kokkos::TeamVectorRange(member, ncellnvar),
	   [&] (int index){

	    //delinearization of the 1D index
	    int ivar = index/(blocksize*blocksize*blocksize);
	    int k = (index-ivar*blocksize*blocksize*blocksize)/(blocksize*blocksize);
	    int j = (index-ivar*blocksize*blocksize*blocksize-k*blocksize*blocksize)/blocksize;
	    int i = (index-ivar*blocksize*blocksize*blocksize-k*blocksize*blocksize-j*blocksize);

	    int kg = k + ghostsize;
	    int jg = j + ghostsize;
	    int ig = i + ghostsize;

	    ghostview(ig,jg,kg,ivar) = mesh.block_at(id).view(i,j,k,ivar);

	  });

	int size = 1;
	for (int il=0; il<(int) key.level; il++) size *=2;

	real_type dleaf = mesh.lbox/size;
	real_type dx = dleaf/blocksize;

	real_type dt = mesh.dt;

	int ilr[3] = {1,0,0}; int jlr[3] = {0,1,0}; int klr[3] = {0,0,1};

	Kokkos::parallel_for
	  (Kokkos::TeamVectorRange(member, ncell),
	   [&] (int index){

	    //delinearization of the 1D index
	    int k = index/(blocksize*blocksize);
	    int j = (index-k*blocksize*blocksize)/blocksize;
	    int i = (index-k*blocksize*blocksize-j*blocksize);

	    int ig = i + ghostsize;
	    int jg = j + ghostsize;
	    int kg = k + ghostsize;

	    Kokkos::Array<real_type,5> Ug;
	    Kokkos::Array<real_type,5> Un;

	    for (int ivar=0; ivar<varsize; ivar++){

	      Ug[ivar] = ghostview(ig,jg,kg,ivar);

	    }

	    real_type rhog = Ug[ID];
	    real_type ug[3] = {Ug[IU]/rhog,
			       Ug[IV]/rhog,
			       Ug[IW]/rhog};
	    real_type ekg  = 0.5*rhog*(ug[0]*ug[0]+ug[1]*ug[1]+ug[2]*ug[2]);
	    real_type pg   = (Ug[IE]-ekg)*(hdparam.gamma-1.);
	    real_type ag   = rhog*sqrt(hdparam.gamma*pg/rhog);

	    for (int idir=0; idir<3; idir++){
	      for (int iface=0;iface<2;iface++){
		
	    	int kn = kg + (2*iface-1)*klr[idir];
	    	int jn = jg + (2*iface-1)*jlr[idir];
	    	int in = ig + (2*iface-1)*ilr[idir];

		for (int ivar=0; ivar<varsize; ivar++){

		  Un[ivar] = ghostview(in,jn,kn,ivar);

		}

	    	real_type rhon = Un[ID];
	    	real_type un[3] = {Un[IU]/rhon,
	    			   Un[IV]/rhon,
	    			   Un[IW]/rhon};
	    	real_type ekn  = 0.5*rhon*(un[0]*un[0]+un[1]*un[1]+un[2]*un[2]);
	    	real_type pn   = (Un[IE]-ekn)*(hdparam.gamma-1.);
	    	real_type an   = rhon*sqrt(hdparam.gamma*pn/rhon);
		  
	    	real_type ustar = 0.; real_type pstar = 0.;
	    	real_type aface = 1.1*fmax(ag,an);
	    	if(iface==1) {
	    	  ustar = 0.5*(ug[idir]+un[idir])-0.5*(pn-pg)/aface;
	    	  pstar = 0.5*(pg+pn)-0.5*(un[idir]-ug[idir])*aface;
	    	} else {
	    	  ustar = 0.5*(ug[idir]+un[idir])-0.5*(pg-pn)/aface;
	    	  pstar = 0.5*(pg+pn)-0.5*(ug[idir]-un[idir])*aface;
	    	}
		
	    	Kokkos::Array<real_type,5> flux;

	    	if((ustar>0. && iface==1)||(ustar<0. && iface==0)) {
	    	  flux[ID] = Ug[ID]*ustar;
	    	  flux[IU] = Ug[IU]*ustar;
	    	  flux[IV] = Ug[IV]*ustar;
	    	  flux[IW] = Ug[IW]*ustar;
	    	  flux[IE] = Ug[IE]*ustar;
 	    	} else {
	    	  flux[ID] = Un[ID]*ustar;
	    	  flux[IU] = Un[IU]*ustar;
	    	  flux[IV] = Un[IV]*ustar;
	    	  flux[IW] = Un[IW]*ustar;
	    	  flux[IE] = Un[IE]*ustar;
	    	}
		
	    	flux[IU+idir] += pstar;
	    	flux[IE] += pstar*ustar;

		 for (int ivar=0; ivar<varsize; ivar++){
		   mesh.block_at(id).view2(i,j,k,ivar) -= (2*iface-1)*dt/dx*flux[ivar];
		  
		 }

	      }
	    }

	  });

    }

  };

  // ************************************************************************
  // Launch WorkTask recursively on all blocks
  struct HdStep {
    using value_type = void;
    using hdparam_type = Param;

    key_type key;
    mesh_type mesh;
    hdparam_type hdparam;

    KOKKOS_INLINE_FUNCTION
    HdStep(key_type key_, mesh_type &mesh_, hdparam_type hdparam_)
      : key(key_), mesh(mesh_), hdparam(hdparam_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type& member) {
      auto& scheduler = member.scheduler();

      Kokkos::single(Kokkos::PerTeam(member), [&]() {

  	  int id = mesh.index_at(key);

  	  if (mesh.leaf_at(id)){

  	    Kokkos::task_spawn
  	      (Kokkos::TaskTeam(scheduler, Kokkos::TaskPriority::High),
  	       HdKernel::HdTask{id,mesh,hdparam});

  	  } else {

	    for (int ichild = 0; ichild<8; ichild++){

  	      key_type key_child =
  		key_type((key.morton<<3) + ichild, key.level+1);

  	      //spawn recursively WorkStep on child leaves
  	      Kokkos::task_spawn(Kokkos::TaskTeam(scheduler),
  	      			 HdKernel::HdStep{key_child,mesh,hdparam});

  	      }

  	  }

  	  });

    }

  };

}

