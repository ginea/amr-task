/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <Kokkos_Core.hpp>
#include <Kokkos_UnorderedMap.hpp>
#include <Kokkos_Array.hpp>
#include <Types.hpp>
#include <io/IniFile.hpp>

// ************************************************************************
// namespace for AMR struct
namespace Amr{

  // ************************************************************************
  // struct for AMR keys, morton code + level
  struct Key {

    morton_type morton;
    level_type level;

    KOKKOS_INLINE_FUNCTION
    Key()
      : morton(),level()
    {}

    KOKKOS_INLINE_FUNCTION
    Key(morton_type morton_,level_type level_)
      : morton(morton_),level(level_)
    {}

    KOKKOS_INLINE_FUNCTION
    Key(Kokkos::Array<int,3> coord_, level_type level_)
      : morton(),level(level_)
    {
      morton_type x = coord_[0];
      morton_type y = coord_[1];
      morton_type z = coord_[2];

      //morton encoding in 32 bits
      x = x & 0x000003FF;
      x = (x | (x << 16)) & 0x030000FF;
      x = (x | (x <<  8)) & 0x0300F00F;
      x = (x | (x <<  4)) & 0x030C30C3;
      x = (x | (x <<  2)) & 0x09249249;

      y = y & 0x000003FF;
      y = (y | (y << 16)) & 0x030000FF;
      y = (y | (y <<  8)) & 0x0300F00F;
      y = (y | (y <<  4)) & 0x030C30C3;
      y = (y | (y <<  2)) & 0x09249249;

      z = z & 0x000003FF;
      z = (z | (z << 16)) & 0x030000FF;
      z = (z | (z <<  8)) & 0x0300F00F;
      z = (z | (z <<  4)) & 0x030C30C3;
      z = (z | (z <<  2)) & 0x09249249;

      morton = x | (y << 1) | (z << 2);

    }

    KOKKOS_INLINE_FUNCTION
    Kokkos::Array<int,3> get_coord()
    {

      Kokkos::Array<int,3> coord;

      //morton decoding in 32 bits
      morton_type x = morton;

      x = x & 0x09249249;
      x = (x | (x >>  2)) & 0x030C30C3;
      x = (x | (x >>  4)) & 0x0300F00F;
      x = (x | (x >>  8)) & 0x030000FF;
      x = (x | (x >> 16)) & 0x000003FF;

      morton_type y = morton >> 1;

      y = y & 0x09249249;
      y = (y | (y >>  2)) & 0x030C30C3;
      y = (y | (y >>  4)) & 0x0300F00F;
      y = (y | (y >>  8)) & 0x030000FF;
      y = (y | (y >> 16)) & 0x000003FF;

      morton_type z = morton >> 2;

      z = z & 0x09249249;
      z = (z | (z >>  2)) & 0x030C30C3;
      z = (z | (z >>  4)) & 0x0300F00F;
      z = (z | (z >>  8)) & 0x030000FF;
      z = (z | (z >> 16)) & 0x000003FF;

      coord[0] = (int) x;
      coord[1] = (int) y;
      coord[2] = (int) z;

      return coord;

    }

    KOKKOS_INLINE_FUNCTION
    Key get_neighbor(int idir, int iface)
    {

      int size = 1;
      for(int il=0; il<(int) level; il++) size *=2;

      Kokkos::Array<int,3> coord = get_coord();

      Kokkos::Array<int,3> coordn;

      if (iface==1) {

	//mask for right neighbors
	int ir[3] = {1,0,0}; int jr[3] = {0,1,0}; int kr[3] = {0,0,1};

	coordn[0] = (coord[0]+ir[idir])<size ? coord[0]+ir[idir] : 0;
	coordn[1] = (coord[1]+jr[idir])<size ? coord[1]+jr[idir] : 0;
	coordn[2] = (coord[2]+kr[idir])<size ? coord[2]+kr[idir] : 0;
      } else {

	//mask for left neighbors
	int il[3] = {-1,0,0}; int jl[3] = {0,-1,0}; int kl[3] = {0,0,-1};

	coordn[0] = (coord[0]+il[idir])>=0 ? coord[0]+il[idir] : size-1;
	coordn[1] = (coord[1]+jl[idir])>=0 ? coord[1]+jl[idir] : size-1;
	coordn[2] = (coord[2]+kl[idir])>=0 ? coord[2]+kl[idir] : size-1;
      }

      Key keyn = Key(coordn,level);

      return keyn;

    }

  };

  // ************************************************************************
  // struct for AMR Blocks, views, futures and leaf mark
  struct Block {

    view_type view;
    view_type view2;
    future_int_type f_AmrMark;
    future_type f_AmrAdapt;
    bool isLeaf;

    KOKKOS_INLINE_FUNCTION
    Block()
      : view(), view2(), isLeaf(true)
    {}

    KOKKOS_INLINE_FUNCTION
    void allocate(int blocksize_, int varsize_, memory_pool &mpool_)
    {
      size_t mem_size = blocksize_*blocksize_*
	blocksize_*varsize_*sizeof(real_type);

      void* ptr_mem = mpool_.allocate(mem_size);
      if (ptr_mem==nullptr)
	Kokkos::abort("need to increase memory_pool capacity");

      void* ptr_mem2 = mpool_.allocate(mem_size);
      if (ptr_mem2==nullptr)
	Kokkos::abort("need to increase memory_pool capacity");

      view = view_type((real_type*)ptr_mem,
		       blocksize_,blocksize_,blocksize_,varsize_);

      view2 = view_type((real_type*)ptr_mem2,
			blocksize_,blocksize_,blocksize_,varsize_);
    }

    KOKKOS_INLINE_FUNCTION
    void deallocate(int blocksize_, int varsize_, memory_pool &mpool_)
    {
      size_t mem_size = blocksize_*blocksize_*
	blocksize_*varsize_*sizeof(real_type);

      mpool_.deallocate(view.data(),mem_size);
      mpool_.deallocate(view2.data(),mem_size);
    }

  };

  // ************************************************************************
  // struct for some parameters for the refinement criterion
  struct RefParam{

    real_type xc;
    real_type yc;
    real_type zc;
    real_type rc;
    real_type vc;
    real_type error_max;

    KOKKOS_INLINE_FUNCTION
    RefParam()
      :xc(0.), yc(0.), zc(0.), rc(0.), vc(0.),error_max(0.)
    {}

    KOKKOS_INLINE_FUNCTION
    RefParam(real_type xc_,
	     real_type yc_,
	     real_type zc_,
	     real_type rc_,
	     real_type vc_,
	     real_type error_max_)
      : xc(xc_), yc(yc_), zc(zc_), rc(rc_), vc(vc_), error_max(error_max_)
    {}

  };

  // ************************************************************************
  // struct for the AMR mesh, a hashmap linking keys and blocks
  template <class MemorySpace>
  struct Mesh{
    using blockmap_type = Kokkos::UnorderedMap<Key,Block>;
    using param_type = RefParam;

    blockmap_type blockmap;
    memory_pool mpool;
    int blocksize;
    int ghostsize;
    int varsize;
    level_type levelmin;
    level_type levelmax;
    real_type lbox;
    param_type refParam;

    int nt;
    real_type dt;
    bool init;

    Mesh()
      : blockmap(), mpool(),
	blocksize(), ghostsize(), varsize(),
	levelmin(),levelmax(),
	lbox(), refParam(), nt(), dt()
    {}

    Mesh(IO::IniFile iParam)
      : blockmap(), mpool(),
	blocksize(), ghostsize(), varsize(),
	levelmin(),levelmax(),
	lbox(), refParam(), nt(), dt()
    {

      blocksize = iParam.get("mesh","blocksize",8);
      ghostsize = iParam.get("mesh","ghostsize",1);
      varsize   = iParam.get("mesh","varsize",1);

      levelmin = iParam.get("mesh","levelmin",3);
      levelmax = iParam.get("mesh","levelmax",3);

      nt   = iParam.get("mesh","nt",100);
      dt   = 0.;
      init = true;

      lbox = iParam.get("mesh","lbox",1.);

      refParam.xc = iParam.get("refinement","xc",0.5);
      refParam.yc = iParam.get("refinement","yc",0.5);
      refParam.zc = iParam.get("refinement","zc",0.5);
      refParam.rc = iParam.get("refinement","rc",0.25);
      refParam.vc = iParam.get("refinement","vc",0.);
      refParam.error_max = iParam.get("refinement","error_max",0.01);

      refParam.xc *= lbox;
      refParam.yc *= lbox;
      refParam.zc *= lbox;
      refParam.rc *= lbox;

      int levelsize = 1;
      for(int il=0;il<levelmax;il++) levelsize *=2;

      int capacity  = levelsize*levelsize*levelsize*4;

      int gblocksize = blocksize+2*ghostsize;

      size_t MinBlockSize  = sizeof(real_type);

      size_t MaxBlockSize  = gblocksize*
	gblocksize*gblocksize*varsize*sizeof(real_type);

      size_t TotalMemory = 2*MaxBlockSize*capacity;

      blockmap = blockmap_type(capacity);

      mpool = memory_pool(MemorySpace(),
			  TotalMemory,
			  MinBlockSize,
			  MaxBlockSize);

    }

    KOKKOS_INLINE_FUNCTION
    int capacity() const
    {
      return blockmap.capacity();
    }

    KOKKOS_INLINE_FUNCTION
    bool valid_at(int index) const
    {
      return blockmap.valid_at(index);
    }

    KOKKOS_INLINE_FUNCTION
    bool valid_at(Key key) const
    {
      return blockmap.exists(key);
    }

    KOKKOS_INLINE_FUNCTION
    bool leaf_at(int index) const
    {
      if(!valid_at(index)) {
	return valid_at(index);
      } else {
	return blockmap.value_at(index).isLeaf;
      }
    }

    KOKKOS_INLINE_FUNCTION
    bool leaf_at(Key key) const
    {
      if(!valid_at(key)) {
	return valid_at(key);
      } else {
	int index = blockmap.find(key);
	return blockmap.value_at(index).isLeaf;
      }
    }

    KOKKOS_INLINE_FUNCTION
    int index_at(Key key) const
    {
      if(!valid_at(key)) {
	Kokkos::abort("invalid key in Mesh.index_at");
      } else {
	return blockmap.find(key);
      }
    }

    KOKKOS_INLINE_FUNCTION
    Key key_at(int index) const
    {
      if(!valid_at(index)) {
	Kokkos::abort("invalid index in Mesh.key_at");
      } else {
	return blockmap.key_at(index);
      }
    }

    KOKKOS_INLINE_FUNCTION
    Block& block_at(int index) const
    {
      if(!valid_at(index)) {
	Kokkos::abort("invalid index in Mesh.block_at");
      } else {
	return  blockmap.value_at(index);
      }
    }

    KOKKOS_INLINE_FUNCTION
    Block& block_at(Key key) const
    {
      if(!valid_at(key)) {
	Kokkos::abort("invalid key in Mesh.block_at");
      } else {
	int index = blockmap.find(key);
	return blockmap.value_at(index);
      }
    }

    KOKKOS_INLINE_FUNCTION
    Kokkos::Array<int,24> neighbors_of(Key key) const
    {

      //array to return with neighbor index
      Kokkos::Array<int,24> index_neighbors;

      //table to check found neighbors
      bool found_neighbors[6] = {false};

      //parent key
      Key key_parent = Key(key.morton>>3,key.level-1);

      //mask to compute the position of the leaf in the parent oct
      int parent_mask[3] = {1,2,4};

      //mask to compute the position of the neighbor child leaves in neighbors
      int child_mask[6][4] = {{1,3,5,7},
			      {0,2,4,6},
			      {2,3,6,7},
			      {0,1,4,5},
			      {4,5,6,7},
			      {0,1,2,3}};


      for (int idir=0; idir<3; idir++){
	for (int iface = 0; iface<2; iface++){

	  //test neighbors at level l-1
	  int parent_position_right = (key.morton & parent_mask[idir]) >> idir;

	  if ((parent_position_right & (iface==1))|
	      (!parent_position_right & (iface==0))){

	    Key key_neigh = key_parent.get_neighbor(idir,iface);

	    if(valid_at(key_neigh)){
	      int id_neigh = index_at(key_neigh);
	      if (leaf_at(id_neigh)){

		found_neighbors[idir*2+iface] = true;
		index_neighbors[idir*2*4+iface*4+0] = id_neigh;
		index_neighbors[idir*2*4+iface*4+1] = -1;
		index_neighbors[idir*2*4+iface*4+2] = -1;
		index_neighbors[idir*2*4+iface*4+3] = -1;

	      }

	    }

	  }

	  //test neighbors at level l
	  if (!found_neighbors[idir*2+iface]){

	    Key key_neigh = key.get_neighbor(idir,iface);

	    if(valid_at(key_neigh)){
	      int id_neigh = index_at(key_neigh);
	      if (leaf_at(id_neigh)){

		found_neighbors[idir*2+iface] = true;
		index_neighbors[idir*2*4+iface*4+0] = id_neigh;
		index_neighbors[idir*2*4+iface*4+1] = -1;
		index_neighbors[idir*2*4+iface*4+2] = -1;
		index_neighbors[idir*2*4+iface*4+3] = -1;

	      }

	    }

	  }

	  //test neighbors at level l+1
	  if (!found_neighbors[idir*2+iface]){

	    Key key_neigh = key.get_neighbor(idir,iface);

	    //loop on neighbor's childs touching current leaf
	    for (int ichild=0; ichild<4; ichild++){

	      Key key_child = Key((key_neigh.morton<<3)+
				  child_mask[idir*2+iface][ichild],
				  key_neigh.level+1);

	      if(valid_at(key_child)){
		int id_neigh = index_at(key_child);
		if (leaf_at(id_neigh)){

		  found_neighbors[idir*2+iface] = true;
		  index_neighbors[idir*2*4+iface*4+ichild] = id_neigh;

		}

	      }

	    }

	  }

	  if (!found_neighbors[idir*2+iface]){
	    Kokkos::abort("A neighbor has not been found in mesh.neighbors_of");

	  }

	}
      }

      return index_neighbors;

    }

    KOKKOS_INLINE_FUNCTION
    Kokkos::Array<int,8> siblings_of(Key key) const
    {

      //array to return with neighbor index
      Kokkos::Array<int,8> index_siblings;

      Key key_parent = Key((key.morton>>3),key.level-1);

      for (int isib=0; isib<8; isib++){

	Key key_sib = Key((key_parent.morton<<3)+isib,key.level);

	int id_sib = index_at(key_sib);

	if (leaf_at(id_sib)){
	  index_siblings[isib] = index_at(key_sib);
	} else {
	   index_siblings[isib] = -1;
	}

      }

      return index_siblings;

    }

    KOKKOS_INLINE_FUNCTION
    bool refinement_at(int id, member_type &member) const
    {

      int refinement = 0;


      Block &block = block_at(id);

      // Key key = key_at(id);
      // Kokkos::Array<int,3> coord = key.get_coord();

      // int size = 1;
      // for (int il=0;il<(int) key.level;il++) size*=2;

      int ncell = blocksize*blocksize*blocksize;

      Kokkos::parallel_reduce
	(Kokkos::TeamVectorRange(member, ncell),
	 [&] (int index, int &inner_ref){

	  //delinearization of the 1D index
	  int kcell = index/(blocksize*blocksize);
	  int jcell = (index-kcell*blocksize*blocksize)/blocksize;
	  int icell = (index-kcell*blocksize*blocksize-jcell*blocksize);

	  // real_type dleaf = lbox/size;
	  // real_type dcell = dleaf/blocksize;

	  // real_type x = coord[0]*dleaf+(icell+0.5)*dcell;
	  // real_type y = coord[1]*dleaf+(jcell+0.5)*dcell;
	  // real_type z = coord[2]*dleaf+(kcell+0.5)*dcell;

	  // real_type dxcloc = 0.;
	  // if (fabs(x-refParam.xc)<fabs(x-(refParam.xc-lbox))){
	  //   dxcloc = fabs(x-refParam.xc);
	  // } else {
	  //   dxcloc = fabs(x-(refParam.xc-lbox));
	  // }

	  real_type err = 0.;
	  real_type eps = 0.01*fabs(block.view(icell,jcell,kcell,ID));

	  //check second derivative error
	  if (kcell>0 && kcell<blocksize-1) {

	    real_type gradl = block.view(icell,jcell,kcell-1,ID)-block.view(icell,jcell,kcell,ID);
	    real_type gradr = block.view(icell,jcell,kcell+1,ID)-block.view(icell,jcell,kcell,ID);
	    		      
	    real_type err_dir = fabs(gradr+gradl)/(fabs(gradl)+fabs(gradr)+eps);
	    err = err_dir>err ? err_dir : err;

	  }

	  if (jcell>0 && jcell<blocksize-1) {

	    real_type gradl = block.view(icell,jcell-1,kcell,ID)-block.view(icell,jcell,kcell,ID);
	    real_type gradr = block.view(icell,jcell+1,kcell,ID)-block.view(icell,jcell,kcell,ID);
				      
	    real_type err_dir = fabs(gradr+gradl)/(fabs(gradl)+fabs(gradr)+eps);
	    err = err_dir>err ? err_dir : err;

	  }

	  if (icell>0 && icell<blocksize-1) {

	    real_type gradl = block.view(icell-1,jcell,kcell,ID)-block.view(icell,jcell,kcell,ID);
	    real_type gradr = block.view(icell+1,jcell,kcell,ID)-block.view(icell,jcell,kcell,ID);
				      
	    real_type err_dir = fabs(gradr+gradl)/(fabs(gradl)+fabs(gradr)+eps);
	    err = err_dir>err ? err_dir : err;

	  }

	  inner_ref += (err>refParam.error_max);

	},refinement);

      return (refinement>0);

    }

  };

}

using mesh_type  = Amr::Mesh<memory_space>;
using block_type = Amr::Block;
using key_type   = Amr::Key;
