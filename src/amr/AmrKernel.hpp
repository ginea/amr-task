/*
//@HEADER
// ************************************************************************
//
// Copyright: Université Paris-Saclay, UVSQ, CNRS, CEA,
// Maison de la Simulation, 91191, Gif-sur-Yvette, France
//
// This software is a computer program whose purpose is to assess the
// performances of the task DAG capability of Kokkos for CFD with AMR.
//
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use,
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info".
//
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability.
//
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or
// data to be ensured and,  more generally, to use and operate it in the
// same conditions as regards security.
//
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
//
// ************************************************************************
//@HEADER
*/

#pragma once

#include <Kokkos_Core.hpp>
#include <Kokkos_UnorderedMap.hpp>
#include <Kokkos_Array.hpp>
#include <amr/AmrCore.hpp>

// ************************************************************************
// namespace containing tasks for AMR cycle
namespace AmrKernel{

  // ************************************************************************
  // Kernel Task for refine/coarsen
  struct AdaptTask {
    using value_type = void;

    int id;
    mesh_type mesh;
    int AmrMark;
    Kokkos::Array<int,24> index_neighbors;
    Kokkos::Array<int, 8> index_siblings;

    future_type f_neigh[6];
    future_type f_neigh_all;

    future_int_type f_sib[8];
    future_type f_sib_all;

    future_type f_adapt[7];
    future_type f_adapt_all;

    int id_child;

    KOKKOS_INLINE_FUNCTION
    AdaptTask(int id_,
	      mesh_type &mesh_,
	      int AmrMark_,
	      Kokkos::Array<int,24> index_neighbors_,
	      Kokkos::Array<int, 8> index_siblings_)
      : id(id_),
  	mesh(mesh_),
	AmrMark(AmrMark_),
	index_neighbors(index_neighbors_),
	index_siblings(index_siblings_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type& member) {
      auto &scheduler = member.scheduler();

      auto blocksize = mesh.blocksize;
      auto blockmap = mesh.blockmap;

      key_type key = mesh.key_at(id);

      //Perform adapt after siblings have been marked
      if(!f_sib_all.is_null()){

	if(AmrMark==-1) {

	  //check if all siblings can be coarsened
	  for (int isib=0; isib<8; isib++){
	    if(!(f_sib[isib].get()==-1)) AmrMark = 0;
	  }
	}

      } else {

	for (int isib=0; isib<8; isib++){

	  int id_sib = index_siblings[isib];

	  if(!(id_sib==-1)){

	    if(!mesh.block_at(id_sib).f_AmrMark.is_null()){

	      f_sib[isib] = mesh.block_at(id_sib).f_AmrMark;

	    } else {

	      Kokkos::single(Kokkos::PerTeam(member), [&]() {
		  Kokkos::respawn(this,scheduler,Kokkos::TaskPriority::Low);
		});
	      return;

	    }

	  }

	}

	Kokkos::single(Kokkos::PerTeam(member), [&]() {
	    f_sib_all = scheduler.when_all(f_sib,8);
	    Kokkos::respawn(this,f_sib_all,Kokkos::TaskPriority::Low);
	  });
	return;

      }

      //if AmrMark = -1, we respawn the last sibling
      //after the other to put the parent oct to leaf
      if ((AmrMark == -1) && (key.morton & 7) == 7) {

	if (f_adapt_all.is_null()){

	  for(int isib=0; isib<7; isib++){

	    int id_sib = index_siblings[isib];
	    if(!mesh.block_at(id_sib).f_AmrAdapt.is_null()){

	      f_adapt[isib] = mesh.block_at(id_sib).f_AmrAdapt;

	    } else {

	      Kokkos::single(Kokkos::PerTeam(member), [&]() {
		  Kokkos::respawn(this,scheduler,Kokkos::TaskPriority::Low);
		});
	      return;

	    }

	  }

	  Kokkos::single(Kokkos::PerTeam(member), [&]() {
	      f_adapt_all = scheduler.when_all(f_adapt,7);
	      Kokkos::respawn(this,f_adapt_all,Kokkos::TaskPriority::Low);
	    });
	  return;

	}

      }

      //perform adapt when after all neighbors have been marked
      if((!f_neigh_all.is_null())||AmrMark==0){

	if (AmrMark == 1){

	  mesh.block_at(id).isLeaf = false;

	  for (int ichild = 0; ichild<8; ichild++){

	    key_type key_child =
	      key_type((key.morton<<3)+ichild, key.level+1);

	    Kokkos::single(Kokkos::PerTeam(member), [&]() {

		block_type block_child;
		block_child.allocate(mesh.blocksize,mesh.varsize,mesh.mpool);

		auto result = blockmap.insert(key_child,block_child);

		if(!result.success())
		  Kokkos::abort("need to increase hashmap capacity");

		id_child = result.index();

	      });

	    member.team_barrier();

	    int ncell = blocksize*blocksize*blocksize;

	    real_type dleaf = 0.; real_type dx = 0.; 
	    Kokkos::Array<int,3> coord;
	    
	    if(mesh.init){

	      int size = 1;
	      for (int il=0; il<(int) key_child.level; il++) size *=2;

	      coord = key_child.get_coord();
	      dleaf = mesh.lbox/size;
	      dx = dleaf/blocksize;

	    }

	    Kokkos::parallel_for
	      (Kokkos::TeamVectorRange(member, ncell),
	       [&] (int index){

		//delinearization of the 1D index
		int k_dst = index/(blocksize*blocksize);
		int j_dst = (index-k_dst*blocksize*blocksize)/blocksize;
		int i_dst = (index-k_dst*blocksize*blocksize-j_dst*blocksize);

		int kblock_pos = (ichild & 4)>>2;
		int jblock_pos = (ichild & 2)>>1;
		int iblock_pos = (ichild & 1);

		int k_src = kblock_pos*blocksize/2 + k_dst/2;
		int j_src = jblock_pos*blocksize/2 + j_dst/2;
		int i_src = iblock_pos*blocksize/2 + i_dst/2;

		if(mesh.init){

		  real_type x = coord[0]*dleaf+(i_dst+0.5)*dx;
		  real_type y = coord[1]*dleaf+(j_dst+0.5)*dx;
		  real_type z = coord[2]*dleaf+(k_dst+0.5)*dx;

		  if (((x-mesh.refParam.xc)*(x-mesh.refParam.xc)+
		       (y-mesh.refParam.yc)*(y-mesh.refParam.yc)+
		       (z-mesh.refParam.zc)*(z-mesh.refParam.zc))<
		    mesh.refParam.rc*mesh.refParam.rc){

		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,ID) = 1.0;
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IE) = 10.0/(1.6667-1.);
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IU) = 0.;
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IV) = 0.;
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IW) = 0.;

		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,ID) = 1.0;
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IE) = 10.0/(1.6667-1.);
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IU) = 0.;
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IV) = 0.;
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IW) = 0.;

		  } else {

		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,ID) = 1.2;
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IE) = 0.1/(1.6667-1.);
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IU) = 0.;
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IV) = 0.;
		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,IW) = 0.;

		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,ID) = 1.2;
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IE) = 0.1/(1.6667-1.);
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IU) = 0.;
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IV) = 0.;
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,IW) = 0.;

		  } 

		} else {

		  for (int ivar=0; ivar<mesh.varsize; ivar ++){

		    mesh.block_at(id_child).view(i_dst,j_dst,k_dst,ivar) =
		      mesh.block_at(id).view(i_src,j_src,k_src,ivar);
		    mesh.block_at(id_child).view2(i_dst,j_dst,k_dst,ivar) =
		      mesh.block_at(id).view2(i_src,j_src,k_src,ivar);
		  }

		}

	      });

	    mesh.block_at(id_child).isLeaf = true;

	  }

	} else if (AmrMark==-1) {

	   key_type key_parent =
	      	key_type(key.morton>>3, key.level-1);

	   int id_parent = mesh.index_at(key_parent);

	   int kblock_pos = (key.morton & 4)>>2;
	   int jblock_pos = (key.morton & 2)>>1;
	   int iblock_pos = (key.morton & 1);

	   int ncell = blocksize/2*blocksize/2*blocksize/2;

	   Kokkos::parallel_for
	     (Kokkos::TeamVectorRange(member, ncell),
	      [&] (int index){

	       //delinearization of the 1D index
	       int k = index/(blocksize/2*blocksize/2);
	       int j = (index-k*blocksize/2*blocksize/2)/(blocksize/2);
	       int i = (index-k*blocksize/2*blocksize/2-j*blocksize/2);

	       int k_dst = kblock_pos*blocksize/2 + k;
	       int j_dst = jblock_pos*blocksize/2 + j;
	       int i_dst = iblock_pos*blocksize/2 + i;

	       for (int ivar=0; ivar<mesh.varsize; ivar++){

		 mesh.block_at(id_parent).view(i_dst,j_dst,k_dst,ivar) = 0.;
		 mesh.block_at(id_parent).view2(i_dst,j_dst,k_dst,ivar) = 0.;

	       }

	       for (int ichild=0; ichild<8; ichild++){

	  	 int k_pos = (ichild & 4)>>2;
	  	 int j_pos = (ichild & 2)>>1;
	  	 int i_pos = (ichild & 1);

	  	 int k_src = 2*k + k_pos;
	  	 int j_src = 2*j + j_pos;
	  	 int i_src = 2*i + i_pos;
		 
		 for (int ivar=0; ivar<mesh.varsize; ivar++){

		   mesh.block_at(id_parent).view(i_dst,j_dst,k_dst,ivar) +=
		     mesh.block_at(id).view(i_src,j_src,k_src,ivar);
		   mesh.block_at(id_parent).view2(i_dst,j_dst,k_dst,ivar) +=
		     mesh.block_at(id).view2(i_src,j_src,k_src,ivar);

		 }

	       }

	       for (int ivar=0; ivar<mesh.varsize; ivar++) {

		 mesh.block_at(id_parent).view(i_dst,j_dst,k_dst,ivar) /= 8;
		 mesh.block_at(id_parent).view2(i_dst,j_dst,k_dst,ivar) /= 8;

	       }

	     });

	  Kokkos::single(Kokkos::PerTeam(member), [&]() {

	      //if last sibling put the parent oct to leaf
	      if ((key.morton & 7) == 7)
		mesh.block_at(id_parent).isLeaf = true;

	      mesh.block_at(id).deallocate(mesh.blocksize,mesh.varsize,mesh.mpool);
	      mesh.block_at(id).isLeaf = false;

	    });

	}

	return;

      }

      Kokkos::single(Kokkos::PerTeam(member), [&]() {

  	  for (int idir=0; idir<3; idir++){
  	    for (int iface=0; iface<2; iface++){

  	      int id_neigh = index_neighbors[idir*2*4+iface*4+0];

  	      if((mesh.key_at(id_neigh).level== key.level-1) |
  		 (mesh.key_at(id_neigh).level== key.level  )){
  		//neighbor at level l or l-1

  		if (!mesh.block_at(id_neigh).f_AmrMark.is_null()){

  		  f_neigh[idir*2+iface] = mesh.block_at(id_neigh).f_AmrMark;

  		} else {

  		  Kokkos::respawn(this,scheduler,Kokkos::TaskPriority::Low);
  		  return;

  		}

  	      } else {
  		// neighbor at level l+1

  		int id_n0 = index_neighbors[idir*2*4+iface*4+0];
  		int id_n1 = index_neighbors[idir*2*4+iface*4+1];
  		int id_n2 = index_neighbors[idir*2*4+iface*4+2];
  		int id_n3 = index_neighbors[idir*2*4+iface*4+3];

  		if (!mesh.block_at(id_n0).f_AmrMark.is_null() &&
  		    !mesh.block_at(id_n1).f_AmrMark.is_null() &&
  		    !mesh.block_at(id_n2).f_AmrMark.is_null() &&
  		    !mesh.block_at(id_n3).f_AmrMark.is_null()){

  		  future_int_type f_all[4] = {mesh.block_at(id_n0).f_AmrMark,
					      mesh.block_at(id_n1).f_AmrMark,
					      mesh.block_at(id_n2).f_AmrMark,
					      mesh.block_at(id_n3).f_AmrMark};

  		  f_neigh[idir*2+iface] = scheduler.when_all(f_all, 4);

  		} else {

  		  Kokkos::respawn(this,scheduler,Kokkos::TaskPriority::Low);
  		  return;

  		}

  	      }

  	    }

  	  }

	  f_neigh_all = scheduler.when_all(f_neigh, 6);
  	  Kokkos::respawn(this,f_neigh_all);

  	});

    }

  };

  // ************************************************************************
  // Kernel Task for AMR mark operations
  struct MarkTask {
    using value_type = int;

    int id;
    mesh_type mesh;

    key_type key;
    Kokkos::Array<int, 3 > coord;
    Kokkos::Array<int, 8 > index_siblings;
    Kokkos::Array<int, 24> index_neighbors;

    bool can_be_refined = true;
    bool can_be_coarsened = true;
    bool to_be_refined = false;

    KOKKOS_INLINE_FUNCTION
    MarkTask(int id_,
	     mesh_type &mesh_)
      : id(id_),
	mesh(mesh_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type &member, int &r_Mark) {
      auto &scheduler = member.scheduler();

      r_Mark = 0;

      key = mesh.key_at(id);
      coord = key.get_coord();
      index_siblings = mesh.siblings_of(key);
      index_neighbors = mesh.neighbors_of(key);

      //check neighbors for 2:1 balance and levelmax
      for (int idir=0; idir<3; idir++){
	for(int iface=0; iface<2; iface++){

	  int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	  key_type key_neigh = mesh.key_at(id_neigh);

	  if((key_neigh.level==(key.level-1)) |
	     (key.level==mesh.levelmax)) can_be_refined = false;

	}
      }

      if(can_be_refined) {

	to_be_refined = mesh.refinement_at(id,member);

	//check refinement in neighbor leaves at l+1
	if (!to_be_refined){

	  for (int idir=0; idir<3; idir++){
	    for(int iface=0; iface<2; iface++){

	      int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	      key_type key_neigh = mesh.key_at(id_neigh);

	      if(key_neigh.level==(key.level+1)){

		for (int ichild=0; ichild<4;ichild++){

		  int id_child = index_neighbors[idir*2*4+iface*4+ichild];

		  if (mesh.refinement_at(id_child,member))
		    to_be_refined=true;

		}
	      }

	    }
	  }

	}


	if (to_be_refined) r_Mark = 1;

      }

      if (!to_be_refined){

	//check coarsening among siblings
	for (int isib=0; isib<8; isib++){

	  if(!mesh.leaf_at(index_siblings[isib]))
	    can_be_coarsened = false;

	}

	//check 2:1 balance among neighbors
	for (int idir=0; idir<3; idir++){
	  for(int iface=0; iface<2; iface++){

	    int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	    key_type key_neigh = mesh.key_at(id_neigh);

	    if((key_neigh.level==(key.level+1)) |
	       (key.level==mesh.levelmin)) can_be_coarsened = false;

	  }
	}

	// check refinement in neighbors at same level
	if(can_be_coarsened) {

	  for (int idir=0; idir<3; idir++){
	    for(int iface=0; iface<2; iface++){

	      int id_neigh = index_neighbors[idir*2*4+iface*4+0];
	      key_type key_neigh = mesh.key_at(id_neigh);

	      if(key_neigh.level==key.level){

		if(mesh.refinement_at(id_neigh,member))
		  can_be_coarsened = false;

	      }

	    }
	  }

	}

	if (can_be_coarsened) r_Mark = -1;

      }

      Kokkos::single(Kokkos::PerTeam(member), [&]() {

	  if(r_Mark != 0){

	    mesh.block_at(id).f_AmrAdapt = Kokkos::task_spawn
	      (Kokkos::TaskTeam(scheduler, Kokkos::TaskPriority::High),
	       AmrKernel::AdaptTask{id,mesh,r_Mark,index_neighbors,index_siblings});

	  }

	});

    }
  };

  // ************************************************************************
  // Create recursively a uniform grid at levelmin
  struct UniformGridStep {
    using value_type = void;

    key_type key;
    mesh_type mesh;

    int id;
    block_type block;

    KOKKOS_INLINE_FUNCTION
    UniformGridStep(key_type key_, mesh_type &mesh_)
      : key(key_), mesh(mesh_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type& member) {
      auto &scheduler = member.scheduler();

      auto blocksize = mesh.blocksize;
      auto blockmap = mesh.blockmap;

      Kokkos::single(Kokkos::PerTeam(member), [&]() {

	  block.allocate(mesh.blocksize,mesh.varsize,mesh.mpool);

	});

      member.team_barrier();

      int size = 1;
      for (int il=0; il<(int) key.level; il++) size *=2;

      Kokkos::Array<int,3>  coord = key.get_coord();
      real_type dleaf = mesh.lbox/size;
      real_type dx = dleaf/blocksize;

      int ncell = blocksize*blocksize*blocksize;
      Kokkos::parallel_for
	(Kokkos::TeamVectorRange(member, ncell),
	 [&] (int index){

	  //delinearization of the 1D index
	  int k = index/(blocksize*blocksize);
	  int j = (index-k*blocksize*blocksize)/blocksize;
	  int i = (index-k*blocksize*blocksize-j*blocksize);

	  real_type x = coord[0]*dleaf+(i+0.5)*dx;
	  real_type y = coord[1]*dleaf+(j+0.5)*dx;
	  real_type z = coord[2]*dleaf+(k+0.5)*dx;

	  if (((x-mesh.refParam.xc)*(x-mesh.refParam.xc)+
	       (y-mesh.refParam.yc)*(y-mesh.refParam.yc)+
	       (z-mesh.refParam.zc)*(z-mesh.refParam.zc))<
	      mesh.refParam.rc*mesh.refParam.rc){

	    block.view(i,j,k,ID) = 1.0;
	    block.view(i,j,k,IE) = 10.0/(1.6667-1.);
	    block.view(i,j,k,IU) = 0.;
	    block.view(i,j,k,IV) = 0.;
	    block.view(i,j,k,IW) = 0.;

	    block.view2(i,j,k,ID) = 1.0;
	    block.view2(i,j,k,IE) = 10.0/(1.6667-1.);
	    block.view2(i,j,k,IU) = 0.;
	    block.view2(i,j,k,IV) = 0.;
	    block.view2(i,j,k,IW) = 0.;

	  } else {

	    block.view(i,j,k,ID) = 1.2;
	    block.view(i,j,k,IE) = 0.1/(1.6667-1.);
	    block.view(i,j,k,IU) = 0.;
	    block.view(i,j,k,IV) = 0.;
	    block.view(i,j,k,IW) = 0.;

	    block.view2(i,j,k,ID) = 1.2;
	    block.view2(i,j,k,IE) = 0.1/(1.6667-1.);
	    block.view2(i,j,k,IU) = 0.;
	    block.view2(i,j,k,IV) = 0.;
	    block.view2(i,j,k,IW) = 0.;
		    
	  } 

	});

      Kokkos::single(Kokkos::PerTeam(member), [&] (){

	  auto result = blockmap.insert(key,block);

	  if(!result.success())
	    Kokkos::abort("need to increase hashmap capacity");

	  id = result.index();

	  if (key.level < mesh.levelmin){

	    mesh.block_at(id).isLeaf = false;

	    for (int ichild = 0; ichild<8; ichild++){

	      key_type key_child =
		key_type((key.morton<<3) + ichild, key.level+1);

	      Kokkos::task_spawn
		(Kokkos::TaskTeam(scheduler),
		 AmrKernel::UniformGridStep{key_child,mesh});
	    }

	  }

	});

    }

  };

  // ************************************************************************
  // Refine/coarsen recursively on all blocks
  struct AmrCycleStep {
    using value_type = void;

    key_type key;
    mesh_type mesh;

    KOKKOS_INLINE_FUNCTION
    AmrCycleStep(key_type key_, mesh_type &mesh_)
      : key(key_), mesh(mesh_)
    {}

    KOKKOS_INLINE_FUNCTION
    void operator()(member_type& member) {
      auto& scheduler = member.scheduler();

      auto blockmap = mesh.blockmap;

      Kokkos::single(Kokkos::PerTeam(member), [&]() {

	  int id = mesh.index_at(key);

	  if (mesh.leaf_at(id)){

	    mesh.block_at(id).f_AmrMark =  Kokkos::task_spawn
	      (Kokkos::TaskTeam(scheduler, Kokkos::TaskPriority::High),
	       AmrKernel::MarkTask{id, mesh});

	  } else {

	    for (int ichild = 0; ichild<8; ichild++){

	      key_type key_child =
		key_type((key.morton<<3) + ichild, key.level+1);

	      //spawn recursively AmrCycle on child leaves
	      Kokkos::task_spawn(Kokkos::TaskTeam(scheduler),
				 AmrKernel::AmrCycleStep{key_child,mesh});

	    }

	  }

	});

    }

  };

}
