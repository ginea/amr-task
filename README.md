# AMR-TASK

This software is a computer program whose purpose is to implement the All-Regime solver on AMR grids with MPI+Kokkos for CFD application.
Compilation options for cmake can be found in the .gitlab-ci.yml file. The parameter file for the test (hard-coded blast) is in the ini directory.
